#!/usr/bin/php5
<?php

require 'Console/Getopt.php';
require 'sync.inc.php';



function disp($source, $dest) {
	global $command,$dest_ext;
	if (!file_exists(dirname($dest))) {
		mkdir(dirname($dest), 0755, true);
	}
	$sourceorig = $source;	
	$tmpfile = ".tmp". uniqid();
	print("Processing: $dest\n");
	//dirname($dest)
	//$tmpdest = ."/". $tmpfile;
	$cmd = sprintf($command, '/tmp'  , escapeshellarg($tmpfile), escapeshellarg($source));
	var_dump($cmd);
	system($cmd);
	if (file_exists($dest)) {
		unlink($dest);
	}
	rename('/tmp/' . $tmpfile . "." . $dest_ext, $dest);
}

if (count($argv) < 2 || count($argv) > 2) {
	die("Usage: sync.php <source> <destination>\n");
}

error_reporting(E_ALL);

$prog = array_shift($argv);
$preset = array_shift($argv);

$config = parse_ini_file("sync.ini", true);

if (!isset($config[$preset])) {
	die("Preset not found\n");
}


$dest_ext = $config[$preset]['extension'];
$source = realpath($config['general']['source']);
$destination = realpath($config[$preset]['destination']);
$command = $config[$preset]['command'];


if (!file_exists($source)) {
	$source = $config[$preset]['source'];
	die("Sorry, directory <$source> needs to exist first.\n");
}
if (!file_exists($destination)) {
	$destination = $config[$preset]['destination'];
	die("Sorry, directory <$destination> needs to exist first.\n");
}
$dest_hash = get_dest_hash($destination);
process_source_with_callbacks($dest_ext, $source, $destination, $dest_hash, "disp");
cleanup($dest_hash);
