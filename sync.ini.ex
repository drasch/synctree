[general]
source=/data/music/source


;[mp3]
;extension=mp3
;destination=/data/music/mp3
;command=pacpl --to mp3 --bitrate 128 --outfile %s %s

[mp3]
extension=mp3
destination=/data/music/mp3
command="pacpl --convertto mp3 --outfile %s --uopts='--preset standard --vbr-new' --file=%s"

;[ogg]
;extension=ogg
;destination=/data/music/ogg
;command=pacpl --to ogg -q 5 --outfile %s %s
