<?php

function get_dest_hash($directory) {
	$exists = array();

	$files = iterator($directory);
	$prefix = strlen($directory);
	foreach ($files as $file) {
		$extension = strrpos($file, '.');
		if ($extension === FALSE) {
			throw new Exception("target file found without extension $file");
		}

		$file_key = substr($file, $prefix+1, $extension-($prefix+1));
		
		$exists[$file_key] = $file;
	}
	return $exists;
}

function process_source_with_callbacks($dest_ext, $directory, $destination, &$dest_hash, $left_right) {
	$files = iterator($directory);
	$prefix = strlen($directory);

	foreach ($files as $file) {
		$extension = strrpos($file, '.');
		if (!is_dir($file) && $extension === FALSE) {
			throw new Exception("source file found without extension $file");
		}
				
		$file_key = substr($file, $prefix+1, $extension-($prefix+1));
		if (!isset($dest_hash[$file_key]) ||  //isn't there
			filemtime($dest_hash[$file_key]) < filemtime($file)) { //needs refreshed
			
			$dest = "$destination/$file_key.$dest_ext";
			disp($file, $dest);
		} 
		unset($dest_hash[$file_key]);
	}
}

function iterator($directory) {
	$output = array();
	exec("find -L $directory -type f -a -not -name '.*' -a -not -name '*.jpg' -a -not -name '*.pdf' -a -not -name '*.m3u' -a -not -name '*.log'", $output);
	return $output;
#$source_rdi = new RecursiveDirectoryIterator($directory);
#	return new RecursiveIteratorIterator($source_rdi);

}

function cleanup($dest_hash) {
	foreach ($dest_hash as $key => $file) {
		if (file_exists($file) && is_file($file)) {
			unlink($file);
		}
	}
}
